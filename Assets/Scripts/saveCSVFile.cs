﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using CustomTypes;
using DG.Tweening;
using UnityEngine.SceneManagement;
using System.Diagnostics;
public class saveCSVFile : MonoBehaviour
{
    public QuistionObject currentQuestionAnswers;
    public EmojisNames namesPrefab;

    private void Start()
    {
        //currentQuestionAnswers = new QuistionObject();
        //currentQuestionAnswers = LocalAccountsManager.Instance.CheckStoredAccounts(0);
    }
    string nme;


    public void SaveCSVfor(int questionNo)
    {
        currentQuestionAnswers = new QuistionObject();

        ES2Spreadsheet sheet = new ES2Spreadsheet();
        currentQuestionAnswers = LocalAccountsManager.Instance.CheckStoredAccounts(questionNo);


        AnswerObject[] tmpAnswers = new AnswerObject[currentQuestionAnswers.AllAnswers.Count];

        int xa = 0;
        for (int i = tmpAnswers.Length - 1; i >= 0; i--)
        {
            tmpAnswers[i] = currentQuestionAnswers.AllAnswers[xa];
            xa++;
        }

        // Add data to cells in the spreadsheet.
        for (int row = 0; row < tmpAnswers.Length; row++)
        {
            AnswerObject tmp = tmpAnswers[row];
   

            for (int col = 0; col < tmp.emojisInMessage.Count; col++)
            {
                sheet.SetCell(col, row, namesPrefab.Names[ tmp.emojisInMessage[col]]);
            }
        }
        switch (questionNo)
        {
            case 0:
                nme = "Which emojis best describe you";
                break;
            case 1:
                nme = "Which emojis best describe Jordan";
                break;
            case 2:
                nme = "Where’s home";
                break;
            case 3:
                nme = "What made you smile today";
                break;
            case 4:
                nme = "Pick one emoji to describe your museum visit so far";
                break;
            default:
                nme = "";
                break;
        }

        sheet.Save( nme + ".csv");


        ShowExplorer("C:\\Emoji's Exported Data\\" + nme + ".csv");

        //string itemPath = "AppData\\LocalLow\\BeeLabs\\Emojies\\ExportedData\\"; // explorer doesn't like front slashes
        //Process.Start("explorer.exe", "" + itemPath);

        // Process.Start(new ProcessStartInfo()
        // {
        //     FileName = "C:\\",//LocalLow\\BeeLabs\\Emojies\\ExportedData\\",
        //     UseShellExecute = true,
        //     Verb = "open"
        //});
    }

    public void ShowExplorer(string itemPath)
    {
        itemPath = itemPath.Replace(@"/", @"\");   
        System.Diagnostics.Process.Start("explorer.exe", "/select," + itemPath);
    }

    public void OpenExplorer()
    {
        System.Diagnostics.Process.Start("explorer.exe", "/start," + "C:\\Emoji's Exported Data\\");
    }


    public void ReturnToGame()
    {
        SceneManager.LoadScene(0);
    }
}
