﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CustomTypes;
public class EmojiTextArea : MonoBehaviour
{

    public Transform textArea;
    public ScrollRect scrollView;
    public List<int> EnterdEmojisNumbers;
    public List<GameObject> TypedEmojis;
    public GameManager mngr;
    public int numberOfEnterdEmojis;
    public Text instText;


    public AnswerObject currentMessage;

    void OnEnable()
    {
        currentMessage = new AnswerObject();
        currentMessage.emojisInMessage = new List<int>();
        numberOfEnterdEmojis = 0;
        mngr.OpenQuestion();

    }

    public void AddChar(Image image, int mo)
    {
        if (mngr.currentQuestion !=4)
        {
            if (numberOfEnterdEmojis < 10)
            {
                instText.gameObject.SetActive(false);
                numberOfEnterdEmojis++;
                GameObject G = new GameObject();
                G.transform.SetParent(textArea);
                G.transform.localScale = Vector3.one;
                G.AddComponent<Image>();
                G.GetComponent<Image>().sprite = image.sprite;
                scrollView.horizontalNormalizedPosition = 1.0f;
                AddShapeToArray(mo);
                TypedEmojis.Add(G);
            }
        }
        else
        {
            if (numberOfEnterdEmojis < 1)
            {
                instText.gameObject.SetActive(false);
                numberOfEnterdEmojis++;
                GameObject G = new GameObject();
                G.transform.SetParent(textArea);
                G.transform.localScale = Vector3.one;
                G.AddComponent<Image>();
                G.GetComponent<Image>().sprite = image.sprite;
                scrollView.horizontalNormalizedPosition = 1.0f;
                AddShapeToArray(mo);
                TypedEmojis.Add(G);
            }
        }

    }

    void AddShapeToArray(int no)
    {
        //for (int i = 0; i < EnterdEmojisNumbers.Count; i++)
        //{
        //    if (EnterdEmojisNumbers[i] == no)
        //    {
        //        return;
        //    }
        //}
        currentMessage.emojisInMessage.Add(no);
    }

    public void SubmitAnswer()
    {
        if (numberOfEnterdEmojis > 0)
        {
            //for (int i = 0; i < EnterdEmojisNumbers.Count; i++)
            //{
            //    PlayerPrefs.SetInt(QuestionNumber + "NumberOfUsesForEmojiNo" + EnterdEmojisNumbers[i], PlayerPrefs.GetInt(QuestionNumber + "NumberOfUsesForEmojiNo" + EnterdEmojisNumbers[i]) + 1);
            //}
            //PlayerPrefs.SetInt(QuestionNumber + "TotalNumberOfUsesEmoji", PlayerPrefs.GetInt(QuestionNumber + "TotalNumberOfUsesEmoji") + EnterdEmojisNumbers.Count);
            try
            {
                bool state = LocalAccountsManager.Instance.IsFileEmpty(LocalAccountsManager.Instance.fileName + "_" + mngr.currentQuestion);

                if (state)
                {
                    Debug.Log("Empty");
                    QuistionObject obj = new QuistionObject();
                    obj.AllAnswers = new List<AnswerObject>();
                    obj.questionNumber = mngr.currentQuestion;
                    obj.AllAnswers.Add(currentMessage);
                    LocalAccountsManager.Instance.AddNewAccount(obj);
                }

                else
                {
                    Debug.Log(" not Empty");
                    QuistionObject obj = LocalAccountsManager.Instance.CheckStoredAccounts(mngr.currentQuestion);
                    Debug.Log("QNo. " + obj.questionNumber);
                    obj.AllAnswers.Add(currentMessage);
                    LocalAccountsManager.Instance.AddNewAccount(obj);
                }
            }
            catch (System.Exception)
            {

                //throw;
                Debug.Log("Error SaveData");
            }


            mngr.OpenChartForQuestion();
            currentMessage.emojisInMessage.Clear();
            numberOfEnterdEmojis = 0;
        }
    }

    public void Backspace()
    {
        if (numberOfEnterdEmojis > 0)
        {
            numberOfEnterdEmojis--;

            currentMessage.emojisInMessage.RemoveAt(currentMessage.emojisInMessage.Count -1);
            Destroy(TypedEmojis[TypedEmojis.Count - 1]);
            TypedEmojis.RemoveAt(TypedEmojis.Count - 1);
        }
        else
        {
            instText.gameObject.SetActive(true);
        }
    }

    public void ClearArea()
    {
        for (int i = 0; i < TypedEmojis.Count; i++)
        {
            Destroy(TypedEmojis[i]);
        }
        instText.gameObject.SetActive(true);
        TypedEmojis.Clear();
        EnterdEmojisNumbers.Clear();
        numberOfEnterdEmojis = 0;
    }


    void OnDisable()
    {
        ClearArea();
    }
}
