﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class EmojiButtonScript : MonoBehaviour
{

    public Image EmojiImg;
    public string EmojiName;
    public int EmojiNo;
    public EmojiTextArea emojisArea;

    void OnEnable()
    {
        EmojiImg.sprite = emojisArea.mngr.Emojis[EmojiNo];
        //transform.localScale = Vector3.zero;
        //transform.DOScale(1, 0.3f).SetDelay(Random.Range(0.10f, 0.30f)).SetEase(Ease.OutBack);
    }

    public void WriteTheEmoji()
    {
        emojisArea.AddChar(EmojiImg, EmojiNo);
    }

}
