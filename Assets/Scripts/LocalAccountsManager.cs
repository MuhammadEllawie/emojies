﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.IO;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using CustomTypes;


public class LocalAccountsManager : MonoBehaviour {

    [Header("Stored Messages references")]

    public string fileName = "Question";

    private static LocalAccountsManager _instance;
    public static LocalAccountsManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = (LocalAccountsManager)GameObject.FindObjectOfType<LocalAccountsManager>();

            return _instance;
        }
    }

 
    public void AddNewAccount(QuistionObject msge) 
    {
        WriteAccountInfo(msge);
    }

    public QuistionObject CheckStoredAccounts(int qNumber)
    {
 
            string filePath = Application.persistentDataPath + "/" + fileName + "_" + qNumber + ".json";

            if (File.Exists(filePath)) 
            {
                string jsonString = File.ReadAllText(filePath);

                if (!string.IsNullOrEmpty(jsonString))
                {
                    QuistionObject savedObj = LoadAccountInfo(jsonString);
                return savedObj;
                } 
            }

        return null; 
     
    }

    private QuistionObject LoadAccountInfo(string stringJson) 
    {
        // Pass the json to JsonUtility, and tell it to create a GameData object from it
        Debug.Log("Saved "+stringJson);
        QuistionObject msge = JsonUtility.FromJson<QuistionObject>(stringJson);
        //TO DO

        return msge;
    }

    public bool IsFileEmpty(string fileName)
    {
        string file = Application.persistentDataPath + "/" + fileName + ".json";

        if (File.Exists(file))
        {
            string jsonString = File.ReadAllText(file);

            if (!string.IsNullOrEmpty(jsonString))
            {
                return false;
            }
        }

        return true;
    }

    private void WriteAccountInfo(QuistionObject msge) 
    {  
        string dataAsJson = JsonUtility.ToJson(msge);

        string newPath = Application.persistentDataPath + "/" + fileName + "_" + msge.questionNumber + ".json";

        string str = dataAsJson.ToString();

        using (FileStream fs = new FileStream(newPath, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(str);
            }
        }
         
        #if UNITY_EDITOR
                UnityEditor.AssetDatabase.Refresh();
        #endif
    }      
}
