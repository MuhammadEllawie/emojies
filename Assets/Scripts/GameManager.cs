﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using UPersian.Components;

public class GameManager : MonoBehaviour
{
    public List<Sprite> Emojis = new List<Sprite>();
    public List<string> Questions = new List<string>();
    public List<string> QuestionsAR = new List<string>();
    public Text QuestionText;
    public DOTweenAnimation QuestionContainer;
    public Text fieldTxt;
    public int currentQuestion;
    public Transform[] Keyboards;

    public Image[] QueImages;

    public Sprite[] SelectedQues;
    public Sprite[] NormalsQues;

    public Text[] ButtonsTexts;
    public Color selectedColor;
    public Color relesedColor;
    public Transform selectorBar;

    public Image nextBTN;
    public Sprite nxtBTNsprite;
    public Sprite replayBTNsprite;
    public Text nxtbtntezxt;
    public Text instText;
    public Image EndImage;

    public Transform KeyboardsHolder;
    [Header("Pages")]
    public GameObject lockScreen;
    public GameObject askScreen;
    public GameObject chartScreen;
    private Coroutine anm;
    Vector3 f = new Vector3();

    void Awake()
    {
        f = QueImages[currentQuestion].transform.localScale;
        //PlayerPrefs.DeleteAll();
        //if (!PlayerPrefs.HasKey("FirstOpen"))
        //{
        //    PlayerPrefs.SetInt("FirstOpen", 0);
        //}

        //if (PlayerPrefs.GetInt("FirstOpen") == 0)
        //{
        //    PlayerPrefs.SetInt("FirstOpen", 1);

        //    for (int x = 0; x < Questions.Count; x++)
        //    {
        //        for (int i = 0; i < 210; i++)
        //        {
        //            if (!PlayerPrefs.HasKey(x + "NumberOfUsesForEmojiNo" + i))
        //            {
        //                PlayerPrefs.SetInt(x + "NumberOfUsesForEmojiNo" + i, 0);
        //            }
        //        }
        //    }

        //    for (int x = 0; x < Questions.Count; x++)
        //    {
        //        if (!PlayerPrefs.HasKey(x + "TotalNumberOfUsesEmoji"))
        //        {
        //            PlayerPrefs.SetInt(x + "TotalNumberOfUsesEmoji", 0);
        //        }
        //    }

        //}
        switchScreens(1);
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(Application.loadedLevel);
    }

    public void switchScreens(int ScreenNo)
    {
        switch (ScreenNo)
        {
            case 1:
                lockScreen.SetActive(true);
                askScreen.SetActive(false);
                chartScreen.SetActive(false);
                break;
            case 2:
                lockScreen.SetActive(false);
                askScreen.SetActive(true);
                chartScreen.SetActive(false);
                break;
            case 3:
                lockScreen.SetActive(false);
                askScreen.SetActive(true);
                chartScreen.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void OpenQuestionByNumber(int number)
    {
        if (number == 0)
        {
            for (int i = 0; i < 5; i++)
            {
                chartScreen.GetComponent<ChartScene>().ticks[i].gameObject.SetActive(false);
            }
        }
        currentQuestion = number;
        OpenQuestion();
    }

    public void OpenQuestion()
    {
        OpenKeyboardByNumber(0);
        if (currentQuestion < Questions.Count)
        {
            if (anm != null)
            {
                StopCoroutine(anm);
                anm = null;
            }
            DOTween.Kill("bns");
            if (currentQuestion + 1 < QueImages.Length)
            {
                QueImages[currentQuestion + 1].transform.DOScale(f, 0.25f);
            }
            if (currentQuestion == Questions.Count - 1)
            {
                nextBTN.sprite = replayBTNsprite;
                nxtbtntezxt.text = "إعادة  Replay";

                fieldTxt.text = "اختيار ١ من الأسفل\nChoose 1 from the below";
            }
            else
            {
                fieldTxt.text = "اختيار 1-10\nSelect 1-10";
            }
            for (int i = 0; i < 5; i++)
            {
                QueImages[i].sprite = NormalsQues[i];
            }
            QueImages[currentQuestion].sprite = SelectedQues[currentQuestion];
            //QueImages[currentQuestion].GetComponent<Button>().interactable = false;
            //if (currentQuestion < 5)
            //{
            //    QueImages[currentQuestion + 1].GetComponent<Button>().interactable = true;
            //}


            instText.gameObject.SetActive(true);
            
            QuestionText.text = QuestionsAR[currentQuestion] + "\n" + Questions[currentQuestion];
            QuestionContainer.DORestart();
            switchScreens(2);
            KeyboardsHolder.localPosition = new Vector3(KeyboardsHolder.localPosition.x, -660, KeyboardsHolder.localPosition.z);
            KeyboardsHolder.DOMoveY(-2.9f, 0.5f).SetEase(Ease.OutCubic);
        }
        else
        {
            currentQuestion = 0;
            for (int i = 0; i < 5; i++)
            {
                QueImages[i].sprite = NormalsQues[i];
            }

            for (int i = 0; i < 5; i++)
            {
                chartScreen.GetComponent<ChartScene>().ticks[i].gameObject.SetActive(false);
            }
            nextBTN.sprite = nxtBTNsprite;
            nxtbtntezxt.text = "السؤال التالي\nNext Question";
            //switchScreens(1);
            ResetScene();
        }
    }

    public void GoNextQuestion()
    {
        currentQuestion++;
        OpenQuestion();
        OpenKeyboardByNumber(0);
    }

    public void OpenChartForQuestion()
    {
        switchScreens(3);
        if (currentQuestion+1 < QueImages.Length)
        {
            StopCoroutine(animatTitle(QueImages[currentQuestion].transform));
            DOTween.Kill("bns");
            anm = null;
            QueImages[currentQuestion + 1].transform.DOScale(f, 0.25f);

            for (int i = 0; i < QueImages.Length; i++)
            {
                //QueImages[i].GetComponent<Button>().interactable = false;
            }
            QueImages[currentQuestion + 1].GetComponent<Button>().interactable = true;

            if (anm == null)
            {
                anm = StartCoroutine(animatTitle(QueImages[currentQuestion+1].transform));
            }
        }
        else
        {
            StartCoroutine(EndScreen());
        }
    }

    public void OpenKeyboardByNumber(int number)
    {
        for (int i = 0; i < Keyboards.Length; i++)
        {
            Keyboards[i].gameObject.SetActive(false);
            ButtonsTexts[i].DOColor(relesedColor, 0.3f);
        }
        Keyboards[number].gameObject.SetActive(true);
        selectorBar.DOMoveX(ButtonsTexts[number].transform.position.x, 0.5f).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            ButtonsTexts[number].DOColor(selectedColor, 0.3f);
        });
    }

    IEnumerator animatTitle(Transform titl)
    {
        yield return new WaitForSeconds(0.75f);
        int i = 0;
        while(i<2)
        {
            QueImages[currentQuestion + 1].transform.DOScale(f + (f * 0.05f), 0.25f).SetId("bns");
            yield return new WaitForSeconds(0.25f);
            QueImages[currentQuestion + 1].transform.DOScale(f, 0.25f).SetId("bns");
            yield return new WaitForSeconds(0.75f);
            i++;
        }
        i = 0;
        anm = null;
    }

    IEnumerator EndScreen()
    {
        Debug.Log("EndScreen");
        yield return new WaitForSeconds(10);
        EndImage.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            Debug.Log("Control Mode, Press E to enter export page");
            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("Trans");
                SceneManager.LoadScene(1);
            }
        }
    }

}
