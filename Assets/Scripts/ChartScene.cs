﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using CustomTypes;
using DG.Tweening;
public class ChartScene : MonoBehaviour
{
    public GameManager mngr;
    public int[] EmojiScores = new int[210];
    public GameObject RecordPrefab;
    public GameObject EmojiPrefab;
    public Transform AnswerdImg;
    public EmojiTextArea txtArea;
    public RectTransform recordsArea;
    public List<EmojiRecordPercent> CalledRecords = new List<EmojiRecordPercent>();
    public QuistionObject currentQuestionAnswers;
    public Image[] ticks;
    Coroutine Calculate;


    void OnEnable()
    {
        currentQuestionAnswers = LocalAccountsManager.Instance.CheckStoredAccounts(mngr.currentQuestion);
        float ytempPos = recordsArea.parent.localPosition.y;
        recordsArea.parent.localPosition = new Vector3(recordsArea.parent.localPosition.x, -460, recordsArea.parent.localPosition.z);
        recordsArea.parent.DOLocalMoveY(ytempPos, 1.0f).SetEase(Ease.OutCubic);

        for (int i = 0; i <= mngr.currentQuestion; i++)
        {
            ticks[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < txtArea.TypedEmojis.Count; i++)
        {
            Transform emoji = txtArea.TypedEmojis[i].transform;
            emoji.transform.SetParent(AnswerdImg);
            emoji.GetComponent<RectTransform>().localPosition = new Vector3(50.0f + (i * 85), 0f, 0f);
            emoji.GetComponent<RectTransform>().sizeDelta = new Vector2(75, 75);
            AnswerdImg.GetComponent<RectTransform>().sizeDelta = new Vector2(100.0f + (i * 85), 4.16f);
        }
        if (Calculate == null)
        {
            Calculate = StartCoroutine(FitchQuestionAnswers());
        }
    }

    IEnumerator FitchQuestionAnswers()
    {
        yield return new WaitForSeconds(0.05f);
        AnswerObject[] tmpAnswers = new AnswerObject[currentQuestionAnswers.AllAnswers.Count];
        int xa = 0;
        for (int i = tmpAnswers.Length - 1; i >= 0; i--)
        {
            tmpAnswers[i] = currentQuestionAnswers.AllAnswers[xa];
            xa++;
        }

        yield return new WaitForSeconds(0.05f);
        for (int i = 0; i < tmpAnswers.Length; i++)
        {
            AnswerObject tmp = tmpAnswers[i];

            GameObject msg = Instantiate(RecordPrefab, recordsArea, false);
            //msg.transform.SetParent(recordsArea);
            //msg.GetComponent<RectTransform>().sizeDelta = new Vector2(100.0f + (tmp.emojisInMessage.Count * 55), 75);
            for (int x = 0; x < tmp.emojisInMessage.Count; x++)
            {
                GameObject emog = Instantiate(EmojiPrefab, msg.transform, false);
                emog.GetComponent<RectTransform>().localPosition = new Vector3(-25 - (x * 60), 0f, 0f);
                emog.GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);
                emog.GetComponent<Image>().sprite = mngr.Emojis[tmp.emojisInMessage[x]];
            }
        }
    }

    /*IEnumerator CalculateCharts()
    {
        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < 211; i++)
        {
            if (i >= mngr.Emojis.Count)
            {
                break;
            }
            EmojiScores[i] = PlayerPrefs.GetInt(mngr.currentQuestion + "NumberOfUsesForEmojiNo" + i);
            float percent = EmojiScores[i] / (float)PlayerPrefs.GetInt(mngr.currentQuestion + "TotalNumberOfUsesEmoji");
            GameObject g = Instantiate(RecordPrefab);
            CalledRecords.Add(g.GetComponent<EmojiRecordPercent>());
            g.GetComponent<EmojiRecordPercent>().gameManager = mngr;
            g.GetComponent<EmojiRecordPercent>().SetPercentage(i, percent, EmojiScores[i]);

        }
        yield return new WaitForSeconds(0.1f);
        EmojiRecordPercent temp = null;

        for (int write = 0; write < CalledRecords.Count; write++)
        {
            for (int sort = 0; sort < CalledRecords.Count - 1; sort++)
            {
                if (CalledRecords[sort].percentage > CalledRecords[sort + 1].percentage)
                {
                    temp = CalledRecords[sort + 1];
                    CalledRecords[sort + 1] = CalledRecords[sort];
                    CalledRecords[sort] = temp;
                }
            }
        }
        CalledRecords.Reverse();
        for (int i = 0; i < CalledRecords.Count; i++)
        {
            CalledRecords[i].transform.SetParent(recordsArea, false);

        }
        yield return new WaitForSeconds(0.1f);
        CalledRecords[0].AdjustFillAmountForAll(true, 0f, 0.5f);
        yield return new WaitForSeconds(0.3f);

        //Array.Sort(EmojiScores);
        //Array.Reverse(EmojiScores);
        Calculate = null;

    }*/

    void OnDisable()
    {
        for (int i = 0; i < txtArea.TypedEmojis.Count; i++)
        {
            Destroy(txtArea.TypedEmojis[i].gameObject);

        }
        txtArea.TypedEmojis.Clear();
        AnswerdImg.GetComponent<RectTransform>().sizeDelta = new Vector2(1, 4.16f);
        Calculate = null;
        CalledRecords.Clear();
        for (int i = 0; i < EmojiScores.Length; i++)
        {
            EmojiScores[i] = 0;
        }
    }

}
