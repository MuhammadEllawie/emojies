﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomTypes
{
    [System.Serializable]
    public class QuistionObject
    {
        public int questionNumber;
        public List<AnswerObject> AllAnswers;
    }


    [System.Serializable]
    public class AnswerObject
    {
        public List<int> emojisInMessage;
    }
}

